# Analysis Methods in Climate Science: Non-parametric Statistics

This GitLab project aims to provide educational resources for learning about non-parametric statistics in the context of climate science analysis. It includes presentation files in the form of Jupyter notebooks and example files for students to go through and gain a better understanding of various non-parametric statistical techniques.

## Topics Covered

1. **Wilcoxon Rank Sum Test**: This presentation introduces the Wilcoxon rank sum test, also known as the Mann-Whitney U test. It explains the concept, assumptions, and steps involved in conducting the test. Students can refer to the accompanying example file to see a practical application of the test using climate science data.

2. **Rank Correlation**: This presentation explores rank correlation methods, focusing on the Spearman rank correlation coefficient. It discusses the significance and interpretation of rank correlation in climate science research. The corresponding example file demonstrates how to calculate and interpret rank correlation using climate data.

3. **Equality of Dispersion**: This presentation covers non-parametric tests for comparing the dispersion or variability between two or more samples. It introduces tests such as the squared-rank test. The example file provides a hands-on demonstration of applying these tests to climate science datasets.

## Data and Examples

unicloud link: https://cloud.uni-graz.at/s/8cYdSq7LRp3PB9c

The project includes a folder containing data files in CSV format. These files are provided as examples for students to work with and apply the non-parametric statistical methods discussed in the presentations. The example files are designed to guide students through the process of loading, analyzing, and interpreting the data using appropriate non-parametric techniques.

## Getting Started

To get started with this project, follow these steps:

1. Clone the repository to your local machine.
2. Explore the presentation files to learn about the different non-parametric statistical methods covered.
3. Open the example files and run them in a Jupyter notebook environment to practice applying the techniques to climate science data.
4. Use the provided data files or experiment with your own datasets to further enhance your understanding of non-parametric statistics.
5. Have a look at the pdf file that shows the location of the measuring location to get an overview of the investigated area.

## Contributing

If you would like to contribute to this project, feel free to submit pull requests with improvements, bug fixes, or additional educational materials related to non-parametric statistics in climate science. Your contributions are highly appreciated!

## License

This project is licensed under the [MIT License](LICENSE). Feel free to use, modify, and distribute the content for educational purposes.

## Contact

For any questions or inquiries about this project, please contact [Andreas Wedenig] at [andreas.wedenig@edu.uni-graz.at] or [Martin Lindlmayer] at [martin.lindlmayer@edu.uni-graz.at]

Enjoy learning non-parametric statistics!
